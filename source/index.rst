.. Economize documentation master file, created by
   sphinx-quickstart on Sun Aug 19 17:40:40 2012.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Documentação do Economize
=========================

Conteúdo:

.. toctree::
   :maxdepth: 2

   ferramentas
   documentacao

Indices e tabelas
=================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

