Ferramentas
===========

*Recipes* para instalação de alguns *softwares* geralmente necessários.

*pip*
-----

O *pip* é utilizado para instalar/desinstalar/atualizar pacotes Python facilmente.

* `Documentação do pip <http://www.pip-installer.org/>`_.

Instalação
~~~~~~~~~~

1. Baixe o arquivo *egg* do *setuptools* de acordo com a versão do seu Python `nesse link <http://pypi.python.org/pypi/setuptools/>`_.
2. Instale-o executando::

    $ sh setuptools-<versao>-py<versao>.egg

3. Baixe o arquivo *tar.gz* do *pip* `nesse link <http://pypi.python.org/pypi/pip/>`_ e extraia seu conteúdo.
4. Instale-o executando::

    $ sh sudo python setup.py install

Instalar pacotes
~~~~~~~~~~~~~~~~

No sistema::

    $ sudo pip install <nome_do_pacote>

Em uma virtualenv::

    $ pip install <nome_do_pacote>

.. warning::

    Mesmo se uma *virtualenv* estiver ativada, um pacote instalado utilizando o *sudo* será instalado no sistema e não na *virtualenv*.

Atualizar pacotes
~~~~~~~~~~~~~~~~~

::

    $ pip install --upgrade <nome_do_pacote>

Desinstalar pacotes
~~~~~~~~~~~~~~~~~~~

::

    $ pip uninstall <nome_do_pacote>

*virtualenv*
------------

*virtualenv* é uma maravilha do mundo moderno que permite criar ambientes python isolados entre si.

* `Documentação do virtualenv <http://www.virtualenv.org/>`_.
* `Documentação do virtualenvwrapper <http://virtualenvwrapper.readthedocs.org/>`_.

Instalação
~~~~~~~~~~

1. Instale o *virtualenv* pelo *pip*::

    $ sudo pip install virtualenv

2. Instale o *virtualenvwrapper* pelo *pip*::

    $ sudo pip install virtualenvwrapper

3. Crie uma pasta no seu *home* chamada *.virtualenvs*::

    $ mkdir ~/.virtualenvs

4. Edite o arquivo *~/.bashrc* (linux) ou *~/.profile* (osx) e cole no final do mesmo::

    export WORKON_HOME=$HOME/.virtualenvs
    source /usr/local/bin/virtualenvwrapper.sh
    export PIP_VIRTUALENV_BASE=$WORKON_HOME
    export PIP_RESPECT_VIRTUALENV=true

5. Feche e abra o console.

Criar nova *virtualenv*
~~~~~~~~~~~~~~~~~~~~~~~

Sem *requirements*::

    $ mkvirtualenv <nome_da_virtualenv>

Com *requirements*::

    $ mkvirtualenv -r requirements.txt <nome_da_virtualenv>

.. note::

    O *requirements.txt* é um arquivo de texto que contém uma lista de pacotes Python. Geralmente existe um *requirements.txt* para cada projeto Python, listando os pré-requisitos do mesmo.

Ativar *virtualenv*
~~~~~~~~~~~~~~~~~~~

::

    $ workon <nome_da_virtualenv>

Desativar *virtualenv*
~~~~~~~~~~~~~~~~~~~~~~

::

    $ deactivate

Atualizar *virtualenv* com requirements
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

::

    $ workon <nome_da_virtualenv>
    $ pip install -r requirements.txt

Extrair *requirements* de uma *virtualenv*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

::

    $ workon <nome_da_virtualenv>
    $ pip freeze > requirements.txt
    $ git add requirements.txt
    $ git commit
