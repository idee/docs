Como modificar a documentação
=============================

Essa página irá explicar como editar e extender a documentação atual.

Baixando
--------

Como baixar a documentação.

*Software* necessário
~~~~~~~~~~~~~~~~~~~~~

A documentação é construída utilizando o *Sphinx*, um pacote Python.

Para instalá-lo basta executar::

    sudo pip install Sphinx

Se quiser que o Sphinx seja visível somente na sua *virtualenv*, instale-o da seguinte forma::

    workon <nome_da_virtualenv>
    pip install Sphinx

Código fonte da documentação
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

O código fonte está hospedado em um repositório específico no *Bitbucket*::

    git clone git@bitbucket.org:idee/docs.git

Editando
--------

Como editar a documentação.

Estrutura
~~~~~~~~~

A raíz da documentação possui alguns arquivos e pastas:

* *source* contém a documentação em si.
* *build* contém o resultado da compilação da documentação.
* *Makefile* e *make.bat* são os arquivos que fazem a compilação.

O *index.rst*
~~~~~~~~~~~~~

Esse é o arquivo principal da documentação e está localizado na pasta *source*.

Ele armazena o `toctree`_ (*table of contents tree*) principal da documentação.

.. _toctree: http://sphinx.pocoo.org/markup/toctree.html

Criando novas páginas 
~~~~~~~~~~~~~~~~~~~~~

Para criar novas páginas basta criar um novo arquivo com extensão **rst** dentro da pasta *source* e listar o seu nome (sem extensão) na *toctree* do *index.rst*.

A sintaxe é bem simples. Você pode se basear nas páginas existentes dessa documentação. É possível acessar o código dessas páginas pelos arquivos **rst** ou online, clicando no link *Show Source*, encontrado na barra esquerda da página.

Mais abaixo estão listados alguns links para a documentação oficial.

Compilando
~~~~~~~~~~

Para compilar a documentação e poder ver o resultado offline (façam isso antes de commitar!) basta executar::

    make html

No Windows o arquivo utilizado é o make.bat, acho que é só executá-lo mas não tenho certeza.

Se a compilação for um sucesso, basta abrir o arquivo *index.html* localizado em *build/html/*.

Documentação do Sphinx
~~~~~~~~~~~~~~~~~~~~~~

* `Documentação geral`_
* `Documentação da sintaxe`_

.. _Documentação geral: http://sphinx.pocoo.org/contents.html
.. _Documentação da sintaxe: http://sphinx.pocoo.org/rest.html

*Deployment*
------------

O *Read the docs* (RTD) é um sistema que hospeda uma documentação *Sphinx* a partir de um repositório. Isso transforma todo o processo de *deployment* para um simples *commit*.

Mas o RTD possui um grave inconveniente: só pode hospedar repositórios públicos. Então cuidado com o conteúdo da documentação pois o mesmo não pode ser privado.
